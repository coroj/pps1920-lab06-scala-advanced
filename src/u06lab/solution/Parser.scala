package u06lab.solution

/** Consider the Parser example shown in previous lesson.
  * Analogously to NonEmpty, create a mixin NotTwoConsecutive,
  * which adds the idea that one cannot parse two consecutive
  * elements which are equal.
  * Use it (as a mixin) to build class NotTwoConsecutiveParser,
  * used in the testing code at the end.
  * Note we also test that the two mixins can work together!!
  */

abstract class Parser[T] {
  def parse(t: T): Boolean  // is the token accepted?
  def end(): Boolean        // is it ok to end here
  def parseAll(seq: Seq[T]): Boolean = (seq forall {parse(_)}) & end() // note &, not &&
}

case class BasicParser(chars: Set[Char]) extends Parser[Char] {
  override def parse(t: Char): Boolean = chars.contains(t)
  override def end(): Boolean = true
}

trait NonEmpty[T] extends Parser[T]{
  private[this] var empty = true
  abstract override def parse(t: T): Boolean = {empty = false; super.parse(t)} // who is super??
  abstract override def end(): Boolean = !empty && {empty = true; super.end()}
}

class NonEmptyParser(chars: Set[Char]) extends BasicParser(chars) with NonEmpty[Char]

trait NotTwoConsecutive[T] extends Parser[T]{
  private[this] var twoConsecutive = false
  private[this] var previous : T = _

  abstract override def parse(t: T): Boolean = {
    if(previous != t) {previous=t; super.parse(t)}
    else {twoConsecutive = true; false}
  }

  abstract override def end(): Boolean = !twoConsecutive && {twoConsecutive = false; super.end()}

}

class NotTwoConsecutiveParser(chars: Set[Char]) extends BasicParser(chars) with NotTwoConsecutive[Char]


object TryParsers extends App {
  def parser = BasicParser(Set('a', 'b', 'c'))
  assert(parser.parseAll("aabc".toList)) // true
  assert(!parser.parseAll("aabcdc".toList)) // false
  assert(parser.parseAll("".toList)) // true

  // Note NonEmpty being "stacked" on to a concrete class
  // Bottom-up decorations: NonEmptyParser -> NonEmpty -> BasicParser -> Parser
  def parserNE = new NonEmptyParser(Set('0','1'))
  assert(parserNE.parseAll("0101".toList)) // true
  assert(!parserNE.parseAll("0123".toList)) // false
  assert(!parserNE.parseAll(List())) // false

  def parserNTC = new NotTwoConsecutiveParser(Set('X','Y','Z'))
  assert(parserNTC.parseAll("XYZ".toList)) // true
  assert(!parserNTC.parseAll("XYYZ".toList)) // false
  assert(parserNTC.parseAll("".toList)) // true

  // note we do not need a class name here, we use the structural type
  def parserNTCNE = new BasicParser(Set('X','Y','Z')) with NotTwoConsecutive[Char] with NonEmpty[Char]
  assert(parserNTCNE.parseAll("XYZ".toList)) // true
  assert(!parserNTCNE.parseAll("XYYZ".toList)) // false
  assert(!parserNTCNE.parseAll("".toList)) // false

  def sparser : Parser[Char] = BasicParser(Set('a','b','c')) // "abc".charParser()
  assert(sparser.parseAll("aabc".toList)) // true
  assert(!sparser.parseAll("aabcdc".toList)) // false
  assert(sparser.parseAll("".toList)) // true

  println("OK")
}


