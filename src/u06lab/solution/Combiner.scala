package u06lab.solution

/**
  * 1) Implement trait Functions with an object FunctionsImpl such that the code
  * in TryFunctions works correctly.
  *
  * 2) To apply DRY principle at the best,
  * note the three methods in Functions do something similar.
  * Use the following approach:
  * - find three implementations of Combiner that tell (for sum,concat and max) how
  *   to combine two elements, and what to return when the input list is empty
  * - implement in FunctionsImpl a single method combiner that, other than
  *   the collection of A, takes a Combiner as input
  * - implement the three methods by simply calling combiner
  *
  * When all works, note we completely avoided duplications..
 */

trait Functions {
  def sum(a: List[Double]): Double
  def concat(a: Seq[String]): String
  def max(a: List[Int]): Int // gives Int.MinValue if a is empty
}

trait Combiner[A] {
  def unit: A
  def combine(a: A, b: A): A
}

object Combiner{

  def create[T](default: T, combineFun: (T,T) => T) : Combiner[T] = new Combiner[T] {
    override def unit: T = default

    override def combine(a: T, b: T): T = combineFun(a,b)
  }

  implicit val SumDoubleCombiner : Combiner[Double] = create(0.0,_ + _)
  implicit val StringConcatCombiner : Combiner[String] = create("",_ + _)
  implicit val IntMaxCombiner : Combiner[Int] = create(Int.MinValue,Math.max)

}


object MyFunctionsImpl {


  def combine[T](seq: Seq[T])(implicit combiner: Combiner[T]) : T =
    seq.fold(combiner.unit)((a,b) => combiner.combine(a,b))

}


object TryFunctions extends App {


  val f2 = MyFunctionsImpl
  println(f2.combine(List(10.0,20.0,30.1))) // 60.1
  println(f2.combine(List[Double]()))                // 0.0
  println(f2.combine(Seq("a","b","c")))   // abc
  println(f2.combine(Seq[String]()))              // ""
  println(f2.combine(List(-10,3,-5,0)))      // 3
  println(f2.combine(List[Int]()))                // -2147483648
}